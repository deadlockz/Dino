#include <SPI.h>
#include <SFE_MicroOLED.h>
#include <avr/power.h>
#include <EEPROM.h>

#define PIN_RESET  9
#define PIN_DC     8
#define PIN_CS    10
#define DC_JUMPER  0

#define BUTTON1     3

#define SPKR        5
#define LED_GREEN   A2

int eeAddress = 0;
int score     = 0;
int highscore = 0;

bool firstBoot = true;

// display 64x48
MicroOLED oled(PIN_RESET, PIN_DC, PIN_CS);

void setup() {  
  pinMode(BUTTON1, INPUT);

  pinMode(SPKR,       OUTPUT);
  pinMode(LED_GREEN,  OUTPUT);
  
  digitalWrite(BUTTON1, HIGH); //default, unpressed =1
  
  power_timer1_disable();
  power_timer2_disable();
  power_adc_disable();
  power_twi_disable();
  
  oled.begin();
  oled.clear(ALL);
  oled.display();
}

void setByte(byte & b, int x, int y) {
  int tmp;
  if (x<0 || x>63 || y<0 || y>47) return;
  for (byte bitNr=0; bitNr<8; ++bitNr) {
    if (((b >> bitNr) & 0x01)) {
      tmp = 7-bitNr+x;
      if (tmp<0 || tmp>63) continue;
      oled.pixel(tmp, y);
    }
  }
}

void setByte90(byte & b, int x, int y) {
  int tmp;
  if (x<0 || x>63 || y<0 || y>47) return;
  for (byte bitNr=0; bitNr<8; ++bitNr) {
    if (((b >> bitNr) & 0x01)) {
      tmp = 7-bitNr+y;
      if (tmp<0 || tmp>47) continue;
      oled.pixel(x, tmp);
    }
  }
}

void gameStart() {
  oled.clear(PAGE);
  EEPROM.get(eeAddress, highscore);

  oled.setCursor(0, 0);
  oled.println("   Dino");
  oled.print(  "==========");
  oled.println("Highscore:");
  oled.print(highscore);
  oled.display();

  delay(3000); // to see the highscore!    
}

void gameOver() {
  if (score > highscore) {
    // store it in a persistent flash ROM
    highscore = score;
    EEPROM.put(eeAddress, highscore);
  }
  digitalWrite(LED_GREEN, LOW);
  analogWrite(SPKR, 5);
  oled.setCursor(0, 0);
  oled.print("0");
  oled.setCursor(0, 22);
  oled.println(" Game Over");
  oled.display();
  delay(1500);
  digitalWrite(SPKR, LOW);
  delay(2000); // read your score
}

void dino(byte y) {
  byte i;
  byte a[] ={
    0b00111000,
    0b00101100,
    0b00111110,
    0b00111110,
    0b00111000,
    0b00111110,
    0b10011000,
    0b01111000,
    0b00111100,
    0b00011000
  };
  for (i=0; i<sizeof(a); ++i) {
    setByte(a[i], 2, i+y);
  }
}

void died() {
  byte i;
  byte a[] ={
    0b00011100,
    0b01111100,
    0b01010100,
    0b01101100,
    0b01010100,
    0b01111100,
    0b00111100,
    0b00001100,
    0b00011100,
    0b00011100,
    0b00001100,
    0b00111100
  };
  for (i=0; i<sizeof(a); ++i) {
    setByte90(a[i], 26-i, 38);
  }
}

void feet1(byte y) {
  byte i;
  byte a[] ={ 
    0b00111000,
    0b00100100
  };
  for (i=0; i<sizeof(a); ++i) {
    setByte(a[i], 2, i+y);
  }
}

void feet2(byte y) {
  byte i;
  byte a[] ={
    0b01111000,
    0b00001000
  };
  for (i=0; i<sizeof(a); ++i) {
    setByte(a[i], 2, i+y);
  }
}

void feet3(byte y) {
  byte i;
  byte a[] ={
    0b00110000,
    0b00101000
  };
  for (i=0; i<sizeof(a); ++i) {
    setByte(a[i], 2, i+y);
  }
}

void printCactus(int & x) {
  byte i;
  int tmp;
  byte a[] = {
    0b00100000,
    0b01100000,
    0b00101000,
    0b10101000,
    0b10101000,
    0b11111000,
    0b01111000,
    0b00111000,
    0b00110000,
    0b00110000,
    0b00100000,
    0b00100000
  };
  for (i=0; i<sizeof(a); ++i) {
    setByte(a[i], x, i+33);
  }
}

void game() {
  score = 0;
  int gamespeed;
  int lives = 3;
  int jumpY = 0;
  int cloud = 56;
  int cactus1 = 70;

  gameStart();
    
  while(lives > 0) {
    // score is time :-D
    score++;
    
    oled.clear(PAGE);
    oled.setCursor(0, 0);
    oled.print(lives);
    oled.print("  ");
    oled.print(score);

    // cloud
    if (cloud == 0) cloud=56;
    oled.setCursor(cloud, 10);
    oled.print("*");
    if (score%2 == 0) cloud--;
      
    // ground
    oled.line(0, 45, 63, 45);

    // cactus
    if (cactus1 < -7) cactus1=70;
    printCactus(cactus1);
    cactus1-=2;


    // collision and die ---------------
    if (cactus1 == 6 && jumpY<13) {
      lives--;
      died();
      
      oled.display();
      digitalWrite(LED_GREEN, LOW);
      analogWrite(SPKR, 120);
      delay(500);
      analogWrite(SPKR, 32);
      delay(500);
      digitalWrite(SPKR, LOW);
      
    } else {
      // a good jump?
      if (jumpY>=13 && cactus1 == 6) score += jumpY;
      
      // alive -------------------------      
      dino(32-jumpY);
      // print the trippling feeds
      if (score%3     == 0) feet1(42-jumpY);
      if ((score+1)%3 == 0) feet2(42-jumpY);
      if ((score+2)%3 == 0) feet3(42-jumpY);
      oled.display();
    }

    // speedup ?!
    if (score > 4000) {
      gamespeed = 10;
    } else if (score > 3000) {
      gamespeed = 20;
    } else if (score > 1500) {
      gamespeed = 30;
    } else if (score > 1000) {
      gamespeed = 40;
    } else if (score > 500) {
      gamespeed = 50;
    } else {
      gamespeed = 70;
    }
    
    delay(gamespeed);

    // jump animation + sound
    switch(jumpY) {          
      case 1: jumpY = 7; analogWrite(SPKR, 60); digitalWrite(LED_GREEN, HIGH); break;
      case 7: jumpY = 11;  break;
      case 11: jumpY = 13; analogWrite(SPKR, 65); break;
      case 13: jumpY = 15; analogWrite(SPKR, 85); break;
      case 15: jumpY = 14;  break;
      case 14: jumpY = 12; analogWrite(SPKR, 95); break;
      case 12: jumpY = 10;  break;
      case 10: jumpY = 9;  digitalWrite(SPKR, LOW); break;
      case 9:  jumpY = 8; break;
      case 8:  jumpY = 6; break;
      case 6: jumpY = 4;  break;
      case 4: jumpY = 2;  digitalWrite(LED_GREEN, LOW); break;
      case 2: jumpY = 0; break;
    }

    // jump button
    if (jumpY==0 && digitalRead(BUTTON1) == LOW) jumpY = 1;

  }
  gameOver();
}



void loop() {
  delay(100);
  if ((digitalRead(BUTTON1) == LOW) || firstBoot) {
    delay(200);
    if ((digitalRead(BUTTON1) == LOW) || firstBoot) {
      oled.command(DISPLAYON);
      power_adc_enable();
      game();
      power_adc_disable();
      oled.command(DISPLAYOFF);
      firstBoot = false;
    }
  }
}


